// take-while //

const even = n => {
  if (n % 2 === 0) return n;
};

const takewhile = (f, arr) => {
  const result = [];

  for (let i = 0; i < arr.length; i++) if (f(arr[i])) result.push(arr[i]);

  return result;
};

console.log(takewhile(even, [2, 4, 7]));

// reduce //

const add = (x, y) => x + y;

const reduce = (f, arr) => {
  let result = 0;
  for (let i = 0; i < arr.length; i++) result = f(result,i);

  return result;
};

console.log(reduce(add, [1, 2, 3]));

// map //
const i = 0;
function inc(i) {
  i += 1;
  return i;
}

const map = (f, arr) => {
  const result = [];
  for (const e of arr) result.push(f(e));
  return result;
};

console.log(map(inc, [2, 3, 4, 5]));



