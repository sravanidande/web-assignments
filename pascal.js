const factorial = (n) =>
{
  let fact= 1
  for(let i=1; i<=n; i++)
    {
      fact = fact*i;
    }
  return fact;
}

console.log(factorial(8));

const ncr=(n,r)=>
 {
  return factorial(n)/(factorial(r)*(factorial(n-r)));
}
 
console.log(ncr(5,2));

const pascalline= (n) =>
{
  const arr=[]
  for(let i=0;i<=n;i++)
    {
      arr.push(ncr(n,i));
 
   }
  console.log(arr);
    
}

console.log(pascalline(4));

const pascaltriangle = (n) =>
{
  const arr=[]
  for (let x=0;x<=n;x++)
    {
      arr.push(pascalline(x));
    }
  console.log(arr);
}

console.log(pascaltriangle(5));
