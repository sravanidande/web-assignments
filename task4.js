// interleave //

const interleave = (arr1, arr2) => {
  const result = [];
  for (const e in arr1) {
    result.push(arr1[e]);
    result.push(arr2[e]);
  }
  return result;
};

console.log(interleave([1, 2, 3], [4, 5, 6]));

// interpose //

const interpose = (arr, s) => {
  const result = [];
  for (const e in arr) result.push(arr[e], s);

  return result;
};

console.log(interpose([2, 3, 4], '/'));


// duplicate //

function distinct(arr) {
  const obj = {};
  const result = [];
  for (let i = 0; i < arr.length; i++) obj[arr[i]] = true;

  for (const e in obj) result.push(e);

  return result;
}

console.log(distinct([1, 2, 2, 3, 3, 3, 4]));



