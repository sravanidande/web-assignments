const reverse = arr => {
  const result = [];
  for (let e = 0; e < arr.length; e++) result[e] = arr[arr.length - e - 1];
  return result;
};

console.log(reverse([22, 23, 24]));
