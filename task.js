const pt = { x: 1, y: 2 };
const clone = obj => {
  const result = {};
  for (let e in obj) result[e] = obj[e];
  return result;
};
console.log(clone(pt));

const assoc = (key, value, obj) => {
  const arr = clone(obj);
  arr[key] = value;
  return arr;
};
console.log(assoc("a", 3, { b: 4, c: 5 }));

function dissoc(obj, key) {
  var result = {};
  for (const y in obj) {
    if (y != key) {
      result[y] = obj[y];
    }
  }
  return result;
}

console.log(dissoc({ x: 1, y: 2, c: 3 }, "y"));

const point = { first: "alice", second: "jake" };
const invert = obj => {
  const result = {};
  for (const e in obj) {
    result[obj[e]] = e;
  }
  return result;
};
console.log(invert(point));

