const evenfn = n => {
  if (n % 2 === 0) return n;
};

const filterfn = (f, arr) => {
  const result = [];
  for (const i of arr) if (f(arr[i])) result.push(arr[i]);
  return result;
};

console.log(filterfn(evenfn, [1, 2, 3, 4]));