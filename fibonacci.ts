export function fibonacci (n: number): number[] {
 
  let x = 1;
   let y = 1;
   const fib = [];
   fib.push(x);
   fib.push(y);
   for (let i = 2; i < n; i++) {
     fib[i] = x + y;
     x = y;
     y = fib[i];
   }
   return fib;
    }


import { fibonacci } from './pnc';

test('fibonacci' , () => {
  expect (fibonacci(5)).toEqual([1, 1, 2, 3, 5]);
  expect (fibonacci(2)).toEqual([1, 1]);
  expect (fibonacci(4)).toEqual([1, 1, 2, 3]);
  expect (fibonacci(6)).toEqual([1, 1, 2, 3, 5, 8]);
});
