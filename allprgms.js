// square all //
function square(arr) {
  let result = [];
  for (let i = 0; i <= arr.length; i++) {
    result.push(arr[i] * arr[i]);
  }
  return result;
}
console.log(square([4, 5, 6]));

// even //
function even(arr) {
  let result = [];
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] % 2 === 0)
      result.push(arr[i]);
  }
  return result;
}
console.log(even([10, 11, 12, 13, 14]));


// array sum //
function arraySum(array) {
  let total = 0,
    len = array.length;

  for (let i = 0; i < len; i++) {
    total += array[i];
  }

  return total;
}
console.log(arraySum([7, 8, 9]));

// max array //
function maximum(arr) {
  for (let i = 1; i < arr.length; i++) {
    if (arr[i] > arr[i + 1])
      return arr[i];
    else
      return arr[i + 1];
  }
}
console.log(maximum([7, 8, 9,]));

// max array //
function maximum(arr) {
  let max = 0;
  let len = arr.length;
  for (let i = 1; i < len; i++) {
    if (arr[i] > max)
      max = arr[i];
  }
  return max;
}
console.log(maximum([67, 78, 89]));

// indexof//
function indexof(array, value) {
  for (let i = 0; i < array.length; i++) {
    if (array[i] === value)
      return i;
      return -1
  }
}
console.log(indexof([2, 3, 4], 3));

// power //
function power(x, y) {
  let result = 1;
  for (let i = 0; i < y; i++) {
    result *= x;
  }
  return result;
}
console.log(power(3, 3));

// factorial //
function Factorial(n) {
  let fact = 1;
  for (let i = 1; i <= n; i++) {
    fact = fact * i;
  }
  return fact;
}
console.log(Factorial(5));

// perfect //
function perfect(n) {
  let sum = 0;
  for (let i = 0; i <= (n - 1); i++) {
    if (n % i === 0)
      sum = sum + i;
  }
  if (sum === n)
    console.log("perfect number");
  else
    console.log("not a perfect number");
}
console.log(perfect(28));

// take //
function take(n, array) {
  let result = [];
  for (let i = 0; i < n; i++) {
    result.push(array[i]);
  }
  return result;
}
console.log(take(2, ([2, 3, 4, 5])));


// reverse //
function reverse(array) {
  let result = [];
  for (let i = 1; i <= array.length; i++) {
    result.push(array[array.length - i]);
  }
  return result;
}

console.log(reverse([44, 45, 46]));


// prime number//

function isPrime(num) {
  if (num === 1)
    return true;
  for (let i = 2; i < num; i++) {
    if (num % i === 0)
      return false;
  }
  return true;
}
console.log(isPrime(19));


// drop //

function drop(n, arr) {
  let result = [];
  for (let i = n; i < arr.length; i++) {
    result.push(arr[i]);
  }
  return result;
}
console.log(drop(3, [1, 2, 3, 4, 5, 6]));


// concat //

function concat(arr1, arr2) {

  for (let i = 0; i < arr2.length; i++) {
    arr1.push(arr2[i]);
  }
  return arr1;
}
console.log(concat([1, 2, 3], [4, 5, 6]));


// pascal //

const factorial = (n) => {
  var fact = 1;
  for (let i = 1; i <= n; i++) {
    fact = fact * i;
  }
  return fact;
};

const ncr = (n, r) => {
  return factorial(n) / (factorial(r) * (factorial(n - r)));
};
const pascalline = (n) => {
  const arr = [];
  for (let i = 0; i <= n; i++) {
    arr.push(ncr(n, i));
  }
  console.log(arr);
}
const pascaltriangle = (n) => {
  const arr = [];
  for (let x = 0; x <= n; x++) {
    arr.push(pascalline(x));
  }
  console.log(arr);
}
console.log(pascaltriangle(5));




// pluck //

const prop = { x: 1, y: 2, z: 3 };
const pluck = (obj, props) => {
  let result = {};
  for (const prop of props) {
    result[prop] = obj[prop]
  }
  return result;
}
console.log(pluck(prop, ["x", "y"]));

// merge//

const pt = { x: 1, y: 2 }
const size = { length: 100, width: 50 }

function merge(obj1, obj2) {
  const result = {};
  for (let i in obj1) { result[i] = obj1[i]; }
  for (let i in obj2) { result[i] = obj2[i]; }
  return result;
}
console.log(merge(pt, size));

// split at //

function splitat(arr, n) {
  let result1 = [];
  let result2 = [];
  for (let i = 0; i < n; i++) { result1.push(arr[i]); }
  for (let i = n; i < arr.length; i++) { result2.push(arr[i]); }
  return [result1, result2]
}
console.log(splitat([1, 2, 3, 4, 5, 6], 3));













